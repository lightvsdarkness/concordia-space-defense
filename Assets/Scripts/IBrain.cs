﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDF
{
    public interface IBrain
    {
        Transform Target { get; }
        Transform _Transform{ get; }
        PlanetSector _PlanetSector { get; }
    }

}