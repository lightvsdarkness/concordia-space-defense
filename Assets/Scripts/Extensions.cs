﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{


    public static void DestroyChildren(this Transform root) {
        int childCount = root.childCount;
        for (int i = 0; i < childCount; i++)
        {
            GameObject.Destroy(root.GetChild(0).gameObject);
        }
        //root.DetachChildren();
    }

    public static void DestroyChildrenImmediate(this Transform root) {
        int childCount = root.childCount;
        for (int i = 0; i < childCount; i++)
        {
            GameObject.DestroyImmediate(root.GetChild(0).gameObject);
        }
        //root.DetachChildren();
    }
}
