﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Default settings.
/// </summary>
public static class GameSettings {
    public static int EnergyPerLevel = 5;
    public static int IndustryPerLevel = 5;

    public static int EnergyCostInIndustry = 15;

    public static int IndustryCostInEnergy = 10;
    public static int IndustryCostInIndustry = 15;


    #region SCENES_REGION
    public static string GameScene = "GameScene";
    #endregion



    #region URLS_REGION
    public static string URL_ANDROID_GAME = "www.google.com";


    public static string URL_ANDROID_MORE_GAMES = "http://www.google.com"; //Link to your googleplay account
    #endregion

    #region PLAYER PREFS
    public static string LEVEL_DATA_PP = "LEVEL_DATA_PP";
    public static string SOUND_STATE_PP = "SOUND_STATE_PP";
    public static string Stats = "Stats";
    #endregion

    #region ADS_REGION
    public static int SHOW_ADS_AFTER_NUMBER_OF_GAMES = 3;
    #endregion

    #region TAGS_REGION
    public static class Tags {
        public static string Destroyable = "Destroyable";
        public static string Enemy = "Enemy";
        public static string Player = "Player";

        public static string CLICKABLE_OBJECT_LAYER = "DestroyableObjectLayer";
    }
    #endregion
}
