﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif


public static class Utilities
{
    /// <summary>
    /// Returns if internet connection is on or off.
    /// </summary>
    public static bool CheckIfInternetConnection() {
        return (Application.internetReachability != NetworkReachability.NotReachable) ? true : false;
    }

#if UNITY_EDITOR
    /// <summary>
    /// Adds a new tag to unity editor.
    /// </summary>
    /// <param name="tagName">Tag name</param>
    public static void AddTag(string tagName) {
        //Load the tag manager files from our project.
        SerializedObject tagManager = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);

        //Ge the tags
        SerializedProperty tags = tagManager.FindProperty("tags");

        //First lets check if the tag that we wanna add exists
        bool tagExists = false;
        for (int i = 0; i < tags.arraySize; i++)
        {
            SerializedProperty tag = tags.GetArrayElementAtIndex(i);

            //If the tag already exits then break
            if (string.Equals(tag.stringValue, tagName))
            {
                Debug.Log("Tag: " + tagName + " already exits. We won't add it twice.");
                tagExists = true;
                break;
            }
        }

        //If we haven't found the tag add it.
        if (tagExists == false)
        {
            //Add a new element to the tag list, set its name and save the changes
            tags.InsertArrayElementAtIndex(0);
            SerializedProperty newTagName = tags.GetArrayElementAtIndex(0);
            newTagName.stringValue = tagName;
            tagManager.ApplyModifiedProperties();
        }
    }

    /// <summary>
    /// Check if the tag exists in our tag manager file.
    /// </summary>
    /// <param name="tagName">The tag name.</param>
    /// <returns>If the tag exists or not</returns>
    public static bool CheckIfTagsExists(string tagName) {
        //Load the tag manager files from our project.
        SerializedObject tagManager = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);

        //Ge the tags
        SerializedProperty tags = tagManager.FindProperty("tags");

        //First lets check if the tag that we wanna add exists
        bool tagExists = false;
        for (int i = 0; i < tags.arraySize; i++)
        {
            SerializedProperty tag = tags.GetArrayElementAtIndex(i);
            //If the tag already exits then break
            if (string.Equals(tag.stringValue, tagName))
            {
                tagExists = true;
                break;
            }
        }

        //Return if it exists or not.
        return tagExists;
    }

    /// <summary>
    /// Ads a layer to project settings
    /// </summary>
    /// <param name="layerName">Layer name to add.</param>
    public static void AddLayer(string layerName) {
        //Load the tag manager files from our project.
        SerializedObject layerManager = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);

        //Get layer properties
        SerializedProperty layersProp = layerManager.FindProperty("layers");

        //Check if we found the layer
        bool foundLayer = false;
        //This is the last added layer
        int lastLayerAdded = 0;

        //check if the layer that we want to add exists
        for (int i = 0; i < layersProp.arraySize; i++)
        {
            SerializedProperty t = layersProp.GetArrayElementAtIndex(i);

            //If it exists then we won't add it anymore
            if (string.Equals(t.stringValue, layerName))
            {
                foundLayer = true;
            }
            else
            {
                //Get the last layer that is not null.
                if (string.IsNullOrEmpty(t.stringValue) == false)
                    lastLayerAdded = i;
            }
        }

        //Set the last layer only if we didn't find the layer that we want to add
        SerializedProperty lastLayer;
        if (foundLayer == false)
        {
            //We are able to modify only the layers from 8 to 32.
            if (lastLayerAdded < 8)
                lastLayerAdded = 7;

            lastLayerAdded++;
            lastLayer = layersProp.GetArrayElementAtIndex(lastLayerAdded);
            lastLayer.stringValue = layerName;
        }

        layerManager.ApplyModifiedProperties();
    }

    /// <summary>
    /// Draw arrow. 
    /// Custom function for OnDrawGizmos.
    /// You can find more on wiki.unity3d.com:
    /// http://wiki.unity3d.com/index.php?title=DrawArrow
    /// </summary>
    /// <param name="pos">Arrow position</param>
    /// <param name="direction">Arrow direction</param>
    /// <param name="color">Arrow Color</param>
    /// <param name="arrowHeadLength">Arrow head length</param>
    /// <param name="arrowHeadAngle">Arrow head angle</param>
    public static void DrawArrow(Vector3 pos, Vector3 direction, Color color, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f) {
        //Set the color for the gizmos
        Gizmos.color = color;
        //First draw a ray from position to direction
        Gizmos.DrawRay(pos, direction);

        //Now draw 2 rays from the first ray head
        //One to the left and the other one to the right.
        Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * new Vector3(0, 0, 1);
        Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * new Vector3(0, 0, 1);
        Gizmos.DrawRay(pos + direction, right * arrowHeadLength);
        Gizmos.DrawRay(pos + direction, left * arrowHeadLength);
    }

    /// <summary>
    /// Draws the camera's frustum.
    /// </summary>
    /// <param name="camera">Camera</param>
    public static void DrawSpawnBoxBasedOnCameraFrustum(Camera camera, float depth, float size) {
        float frustumHeight = 2 * depth * Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad);

        float frustumWidht = frustumHeight * camera.aspect;
        float furustmHeight_ = frustumWidht / camera.aspect;
        Gizmos.DrawWireCube((camera.transform.position + new Vector3(0f, 0f, depth)), new Vector3(frustumWidht, furustmHeight_, size));
    }

    /// <summary>
    /// Returns the intersetction between 3 planes
    /// </summary>
    /// <param name="p1">Plane 1</param>
    /// <param name="p2">Plane 2</param>
    /// <param name="p3">Plane 3</param>
    /// <returns></returns>
    static Vector3 Plane3Intersect(Plane p1, Plane p2, Plane p3) {
        //get the intersection point of 3 planes
        return ((-p1.distance * Vector3.Cross(p2.normal, p3.normal)) +
                (-p2.distance * Vector3.Cross(p3.normal, p1.normal)) +
                (-p3.distance * Vector3.Cross(p1.normal, p2.normal))) /
            (Vector3.Dot(p1.normal, Vector3.Cross(p2.normal, p3.normal)));
    }

    /// <summary>
    /// Layer mask field for editor.
    /// </summary>
    /// <param name="label">label string</param>
    /// <param name="layerMask">Layer mask</param>
    /// <returns>Returns the selected layer mask.</returns>
    public static LayerMask LayerMaskField(string label, LayerMask layerMask) {
        //set the layers
        List<string> layers = new List<string>();
        List<int> layerNumbers = new List<int>();

        //Get all the layers that we have
        for (int i = 0; i < 32; i++)
        {
            string layerName = LayerMask.LayerToName(i);
            if (layerName != "")
            {
                layers.Add(layerName);
                layerNumbers.Add(i);
            }
        }
        //Remove empty layers.
        int maskWithoutEmpty = 0;
        for (int i = 0; i < layerNumbers.Count; i++)
        {
            if (((1 << layerNumbers[i]) & layerMask.value) > 0)
                maskWithoutEmpty |= (1 << i);
        }
        //Return selected
        maskWithoutEmpty = EditorGUILayout.MaskField(label, maskWithoutEmpty, layers.ToArray());
        int mask = 0;
        for (int i = 0; i < layerNumbers.Count; i++)
        {
            if ((maskWithoutEmpty & (1 << i)) > 0)
                mask |= (1 << layerNumbers[i]);
        }
        layerMask.value = mask;
        return layerMask;
    }
#endif
}