﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDF {
    public class PlanetSector : MonoBehaviour, IDamageable {
        public int SectorNumber;
        public float HealthInitial = 10000;
        public float Health = 10000;
        float IDamageable.Health => Health;

        public float LastTickLossess = 1;

        public Transform VisualsIndustryParent;
        public GameObject CurrentIndustryVisuals;

        public Transform VisualsPowerPlantParent;
        public GameObject CurrentPowerPlantVisuals;

        public Transform VisualsWeaponParent;
        public GameObject CurrentWeaponVisuals;

        public Industry _Industry;
        public PowerPlant _PowerPlant;

        public int WeaponLevel = 0;

        public Transform _Transform => transform;

        void Awake() {
            if (_Industry == null)
                _Industry = GetComponent<Industry>();
            if (_PowerPlant == null)
                _PowerPlant = GetComponent<PowerPlant>();
        }

        void Start() {
            VisualizePowerPlant();
            VisualizeIndustry();
        }


        public void VisualizeIndustry() {
            //int childCount = VisualsIndustryParent.childCount;
            //for (int i = childCount - 1; i >= 0; i--) {
            //    Destroy(VisualsIndustryParent.GetChild(i).gameObject);
            //}
            VisualsIndustryParent.DestroyChildren();

            switch (_Industry.Level) {
                case 1:
                    CurrentIndustryVisuals = Instantiate(ManagerPlanet.Instance.PrefabIndustryI, VisualsIndustryParent.position, Quaternion.identity, VisualsIndustryParent);
                    break;
                case 2:
                    CurrentIndustryVisuals = Instantiate(ManagerPlanet.Instance.PrefabIndustryII, VisualsIndustryParent.position, Quaternion.identity, VisualsIndustryParent);
                    break;
                case 3:
                    CurrentIndustryVisuals = Instantiate(ManagerPlanet.Instance.PrefabIndustryIII, VisualsIndustryParent.position, Quaternion.identity, VisualsIndustryParent);
                    break;
            }
        }


        [ContextMenu("DestroyIndustry")]
        public void DestroyIndustryI() {
            VisualsIndustryParent.DestroyChildrenImmediate();
        }
        [ContextMenu("BuildIndustryI")]
        public void BuildIndustryI() {
            VisualsIndustryParent.DestroyChildrenImmediate();
            CurrentIndustryVisuals = Instantiate(ManagerPlanet.Instance.PrefabIndustryI, VisualsIndustryParent);
        }

        public void VisualizePowerPlant() {
            VisualsPowerPlantParent.DestroyChildren();

            switch (_Industry.Level)
            {
                case 1:
                    CurrentPowerPlantVisuals = Instantiate(ManagerPlanet.Instance.PrefabPowerPlantI, VisualsPowerPlantParent.position, Quaternion.identity, VisualsPowerPlantParent);
                    break;
                case 2:
                    CurrentPowerPlantVisuals = Instantiate(ManagerPlanet.Instance.PrefabPowerPlantII, VisualsPowerPlantParent.position, Quaternion.identity, VisualsPowerPlantParent);
                    break;
                case 3:
                    CurrentPowerPlantVisuals = Instantiate(ManagerPlanet.Instance.PrefabPowerPlantIII, VisualsPowerPlantParent.position, Quaternion.identity, VisualsPowerPlantParent);
                    break;
            }
        }


        public void ReceiveDamage(float damageAmount, Vector3 hitPosition) {
            Health -= damageAmount;

            var currentLosses = Health / HealthInitial;
            //Debug.Log("currentLosses: " + currentLosses);
            var delta = LastTickLossess - currentLosses;

            var LivesLost = ManagerPlanet.Instance.PopulationPointsInitial * delta / 8f;
            //Debug.Log("LivesLost: "+ LivesLost);
            ManagerPlanet.Instance.PopulationPoints -= (long) LivesLost;
            ManagerPlanet.Instance.UpdatePopulation();
        }

        public void ReceiveHeal(float healAmount, Vector3 hitPosition) {

        }
    }
}