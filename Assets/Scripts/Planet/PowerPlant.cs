﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerPlant : MonoBehaviour
{
    public int Level;

    void Start()
    {
        
    }


    public void AddLevel() {
        Level++;
    }
    public void ReduceLevel() {
        Level--;
    }
    public int CalculateEnergy() {
        return Level * GameSettings.EnergyPerLevel;
    }
}
