﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDF {
    public class ManagerPlanet : SingletonManager<ManagerPlanet> {
        public int IndustryPoints = 5;
        public int EnergyPoints = 10;
        public long PopulationPointsInitial = 6000000000;
        public long PopulationPoints = 6000000000;

        public List<PlanetSector> Sectors = new List<PlanetSector>();

        public List<Industry> Industries = new List<Industry>();
        public List<PowerPlant> PowerPlants = new List<PowerPlant>();

        public GameObject PrefabIndustryI;
        public GameObject PrefabIndustryII;
        public GameObject PrefabIndustryIII;
        public GameObject PrefabPowerPlantI;
        public GameObject PrefabPowerPlantII;
        public GameObject PrefabPowerPlantIII;

        private Coroutine _CalcTick;

        protected override void Start() {
            base.Start();

            PopulationPoints = PopulationPointsInitial;

            ManagerUI.Instance.TextIndustryPoints.text = IndustryPoints.ToString();
            ManagerUI.Instance.TextEnergyPoints.text = EnergyPoints.ToString();


            _CalcTick = StartCoroutine(CalcTick());
        }
        void OnDisable() {
            if (_CalcTick != null)
                StopCoroutine(_CalcTick);
        }
        //void Update() {

        //}

        IEnumerator CalcTick() {
            while (true) {
                if (Debugging)
                    Debug.Log("CalcTick");
                yield return new WaitForSeconds(5f);

                EnergyPoints = 0;
                foreach (var powerPlant in PowerPlants)
                {
                    EnergyPoints += powerPlant.CalculateEnergy();
                }
                if (Debugging)
                    Debug.Log("PlusEnergy" + EnergyPoints);

                var minusEnergy = 0;
                foreach (var industry in Industries)
                {
                    minusEnergy -= industry.CalculateCostInEnergy();
                    IndustryPoints += industry.CalculateIndustry();
                }
                EnergyPoints += minusEnergy;
                if (Debugging)
                    Debug.Log("MinusEnergy" + minusEnergy);

                PopulationPoints = (long)(PopulationPoints * 1.015f);
                UpdatePopulation();

                UpdateIndustryAndEnergy();
                if (Debugging)
                    Debug.Log("CalcTick end");
            }

        }

        public void UpdatePopulation() {
            ManagerUI.Instance.TextPopulationPoints.text = PopulationPoints.ToString();
        }
        public void UpdateIndustryAndEnergy() {
            if (Debugging)
                Debug.Log("IndustryPoints: " + IndustryPoints);
            ManagerUI.Instance.TextIndustryPoints.text = IndustryPoints.ToString();
            ManagerUI.Instance.TextEnergyPoints.text = EnergyPoints.ToString();
        }

        public void AddIndustry(int sectorNumber) {
            var projectedEnergyPoints = EnergyPoints - GameSettings.IndustryCostInEnergy;
            var projectedIndustryPoints = IndustryPoints - GameSettings.IndustryCostInIndustry;

            if (projectedEnergyPoints < 0) {
                if (Debugging)
                    Debug.Log("Adding Industry - Failure: Not Enough Energy");
            }

            if (projectedIndustryPoints < 0) {
                if (Debugging)
                    Debug.Log("Adding Industry - Failure: Not Enough Industry");
            }

            if (projectedEnergyPoints >= 0 && projectedIndustryPoints >= 0) {
                if (Debugging)
                    Debug.Log("Adding Industry - Success");
                Industries[sectorNumber].AddLevel();

                IndustryPoints = projectedIndustryPoints;
                UpdateIndustryAndEnergy();

                Sectors[sectorNumber].VisualizeIndustry();
            }

        }

        
        public void AddPowerPlant(int sectorNumber) {
            var projectedIndustryPoints = IndustryPoints - GameSettings.EnergyCostInIndustry;

            
            if (projectedIndustryPoints < 0)
            {
                if (Debugging)
                    Debug.Log("Adding PowerPlant - Failure: Not Enough Industry");
            }

            if (projectedIndustryPoints >= 0)
            {
                if (Debugging)
                    Debug.Log("Adding PowerPlant - Success");
                PowerPlants[sectorNumber].AddLevel();

                IndustryPoints = projectedIndustryPoints;
                UpdateIndustryAndEnergy();

                Sectors[sectorNumber].VisualizePowerPlant();
            }
        }

        public void AddWeaponParticleBeam(int sectorNumber) {
            Sectors[sectorNumber].WeaponLevel++;
        }

    }
    
}