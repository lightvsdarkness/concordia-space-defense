﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDF {
    public class TurretWeapon : MonoBehaviour {
        public bool Debugging;

        [Space]
        public bool Online = true;
        public WeaponType _WeaponType;

        [Header("Characteristics")]
        [SerializeField] private float rotationalDamp = 1.5f;

        [Header("Links")]
        public IBrain _Brain;
        public IWeapon _Weapon;

        [SerializeField] private Transform _targetTransform;
        [SerializeField] private Vector3 _targetPosition;

        private Coroutine ContinuousTurnRef = null;

        private void Start() {
            if (_Weapon == null)
                _Weapon = GetComponentInChildren<IWeapon>();
            if (_Brain == null)
                _Brain = GetComponentInParent<IBrain>();
        }
        //private void Update() {
        //    Debug.DrawLine(transform.position, transform.position + transform.forward, Color.black);
        //}

        public bool InFront(Vector3 targetPosition) {
            Vector3 directionToTarget = _Brain._Transform.position - targetPosition;
            float angle = Vector3.Angle(_Brain._Transform.forward, directionToTarget);

            if (Mathf.Abs(angle) > 105 && Mathf.Abs(angle) < 255)
            {
                //Debug.DrawLine(transform.position, _Brain.Target.position, Color.green);
                return true;
            }

            //Debug.DrawLine(transform.position, _Brain.Target.position, Color.yellow);
            return false;
        }

        public bool HaveLineOfSight(Vector3 targetPosition) {
            RaycastHit hit;
            //Debug.Log(_Brain.Target.name + " " + _Brain.Target.position);
            Vector3 direction = targetPosition - _Weapon._Transform.position;


            if (Physics.Raycast(_Weapon._Transform.position, direction, out hit, _Weapon.MaxDistance, ManagerInput.Instance.MaskExceptBoard))
            {
                if (hit.transform.CompareTag(GameSettings.Tags.Enemy))
                {
                    Debug.DrawRay(_Weapon._Transform.position, direction, Color.red);
                    return true;
                }
                else
                    Debug.DrawRay(_Weapon._Transform.position, direction, Color.white);
                //Debug.Log("HaveLineOfSight targeting: " + _Brain.Target.name + " " + _Brain.Target.position); //targetPosition
                //Debug.Log("HaveLineOfSight hitting: " + hit.transform.name);
            }
            else {
                //Debug.DrawRay(_Weapon._Transform.position, direction, Color.black);
            }

            return false;
        }

        public void StopAttacks() {
            _Weapon.StopAttack();
        }

        /// <summary>
        /// 
        /// </summary>
        private void TryAttack(Vector3 targetPosition) {
            if (_WeaponType == WeaponType.Railgun || _WeaponType == WeaponType.ParticleBeam) {
                //Debug.Log("Turret InFront: " + InFront());
                //Debug.Log("Turret HaveLineOfSight: " + HaveLineOfSight());

                if (InFront(targetPosition) && HaveLineOfSight(targetPosition)) {
                    //Debug.Log("Turret Railgun or ParticleBeam Firing _Weapon");
                    _Weapon.TryFireWeapon(targetPosition, _Brain._PlanetSector.WeaponLevel * 10);
                }
            }
            else {
                //Debug.Log("Turret Firing Some _Weapon");
                _Weapon.TryFireWeapon(targetPosition, _Brain._PlanetSector.WeaponLevel * 10);
            }

        }

        //public void Turn(Vector3 targetPosition) {
        //    if (ContinuousTurnRef != null) {
        //        StopCoroutine(ContinuousTurnRef);
        //    }
        //    if (InFront(targetPosition))
        //    {
        //        Vector3 pos = targetPosition - transform.position;
        //        Quaternion rotation = Quaternion.LookRotation(pos);
        //        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationalDamp * Time.deltaTime);
        //    }
        //}

        public void ContinuousTurn(Transform targetTransform) {
            _targetTransform = targetTransform;
            _targetPosition = targetTransform.position;

            if (ContinuousTurnRef != null) {
                StopCoroutine(ContinuousTurnRef);
            }
            if (InFront(_targetPosition)) {
                if (Debugging)
                    Debug.Log("Starting ContinuousTurnCor at: " + _targetPosition);
                ContinuousTurnRef = StartCoroutine(ContinuousTurnCor(targetTransform));
            }
        }
        IEnumerator ContinuousTurnCor(Transform targetTransform) {
            Debug.Log("Laser of " + transform.parent.parent + "Attack: " + targetTransform.name);

            var targetTransformLink = targetTransform;

            while (Online && targetTransformLink != null) {
                
                //Vector3 pos = transform.InverseTransformPoint(targetPosition) - transform.position;
                Vector3 pos = targetTransformLink.position - transform.position;
                Quaternion rotation = Quaternion.LookRotation(pos);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationalDamp * Time.deltaTime);
                if (Debugging)
                    Debug.Log("Vector of ContinuousTurnCor: " + pos);

                // NOTE: Теория: стрельба происходит слишком рано
                // If can attack and don't attack now
                if (HaveLineOfSight(targetTransformLink.position)) {
                    // 
                    TryAttack(targetTransformLink.position);
                }
                yield return null;
            }

        }
    }
}