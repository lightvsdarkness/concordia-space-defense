﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

namespace SDF {
    public class TurretBrain : MonoBehaviour, IBrain {
        [SerializeField] private Transform _target;
        Transform IBrain.Target => _target;

        public PlanetSector _PlanetSector;
        PlanetSector IBrain._PlanetSector => _PlanetSector;

        public bool Automatic = false;

        public GameObject TargetPrefab;

        public TurretWeapon _TurretWeapon;

        public Transform _Transform => transform;

        //
        //private GameObject inst;


        private void Start() {
            GetRefs();
        }
        [ContextMenu("GetRefs")]
        public void GetRefs() {
            if (_PlanetSector == null)
                _PlanetSector = GetComponentInParent<PlanetSector>();
            if (_TurretWeapon == null)
                _TurretWeapon = GetComponentInChildren<TurretWeapon>();

        }

        private void Update() {
            //Debug.DrawLine(transform.position, transform.position + transform.forward, Color.yellow);
            if (Input.GetMouseButtonDown(0))
            { //GetButtonDown("Fire1")
                RaycastAndCheckPointerClick();

            }
            // Check if there is a touch
            else if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                // Check if finger is over a UI element
                if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)) {
                    Debug.Log("Touched the UI");
                }
            }
            else if (Automatic && _target != null) {
                //

            }

        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns>True if target was created</returns>
        private bool RaycastAndCheckPointerClick() {
            // Check if finger is over a UI element
            //if (EventSystem.current.currentSelectedGameObject != null) return;
            if (EventSystem.current.IsPointerOverGameObject()) {
                Debug.LogWarning("TurretBrain: Pointer is over UI");
                return false;
            }

            // Stop previous firing
            _target = null;
            _TurretWeapon.StopAttacks();

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            // TRY HIT TARGET
            // First try to hit Enemy IDamageable and if there is no such object - create instance of TargetPrefab
            if (Physics.Raycast(ray, out hit, 100f, ManagerInput.Instance.MaskInteractive))
            {
                IDamageable interactiveObject = hit.transform.GetComponent<IDamageable>();
                if (interactiveObject != null) {
                    Debug.LogWarning("TurretBrain: We hit interactive");
                    _target = interactiveObject._Transform;

                }
                

                _TurretWeapon.ContinuousTurn(hit.transform); // Maybe another????
                return true;
            }


            // HIT BOARD
            Physics.Raycast(ray, out hit, 100f, ManagerInput.Instance.MaskBoard);

            //Target = Instantiate(TargetPrefab, hit.point, Quaternion.identity).transform;
            //inst = Instantiate(TargetPrefab, hit.point, Quaternion.identity);
            //Target = inst.transform;
            Debug.LogWarning("TurretBrain: We hit board");

            //_TurretWeapon.ContinuousTurn(hit.point);
            return true;

        }


        public Transform FindTarget() {
            var dfkj = FindObjectsOfType<EnemyMain>();
            return dfkj.First().transform;
        }


    }
}