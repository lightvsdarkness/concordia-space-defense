﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Industry : MonoBehaviour {
    public int Level;

    void Start()
    {
        
    }


    public void AddLevel() {
        Level++;
    }
    public void ReduceLevel() {
        Level--;
    }

    public int CalculateIndustry() {
        return Level * GameSettings.IndustryPerLevel;
    }

    public int CalculateCostInEnergy() {
        return Level * GameSettings.IndustryCostInEnergy;
    }
}
