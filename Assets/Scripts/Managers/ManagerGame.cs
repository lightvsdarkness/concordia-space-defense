﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SDF {
    public class ManagerGame : SingletonManager<ManagerGame> {




        public bool gameOver = false; //is the game over?
        public bool continuedGame = false; //Did the user watch a video to continue the game? 
        //private bool isTutorialOn = false; //Is the tutorial on?



        void Update() {
            if (ManagerPlanet.Instance.PopulationPoints <= 0)
                Restart();
        }

        public void Restart() {
            Debug.LogWarning("PopulationPoints: " + ManagerPlanet.Instance.PopulationPoints);
            Debug.LogWarning("Restart Level");

            //if (FadeScreenManager.singleton != null)
            //{
            //    FadeScreenManager.singleton.FadeIntoScene(GameSettings.GAME_SCENE);
            //}
            //else
            //{
            //    SceneManager.LoadScene(GameSettings.GAME_SCENE);
            //}
            SceneManager.LoadScene(GameSettings.GameScene);
        }

        public void Exit() {
            Debug.LogWarning("Quit Game");

            Application.Quit();
        }
    }
}