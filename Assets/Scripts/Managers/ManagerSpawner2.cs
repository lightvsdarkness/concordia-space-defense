﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDF
{
    public class ManagerSpawner2 : MonoBehaviour {

    //public AIGroup[] waves;
    public int EnemiesInWave;
    public GameObject EnemyPrefab;
    public Transform[] spawnPoints;

    [SerializeField] private int currentWave = 0;
    public int enemiesLeft = 0;

    public bool spawnerActive = true;
    float timeTilNextWave = 0;
    public float timeBeforeFirstWave = 1;
    public float timeBetweenWaves = 3;

    public float spawnPointDiameter = 10.0f;


    void OnEnable() {
        timeTilNextWave = timeBeforeFirstWave;
        StartCoroutine(StartWave());
    }

    void OnDisable() {
        StopCoroutine(StartWave());
    }

    IEnumerator StartWave() {
        while (spawnerActive)
        {
            if (timeTilNextWave < 0 && enemiesLeft <= 0 && currentWave < 99)
            {
                for (int i = 0; i < EnemiesInWave; i++)
                {
                    Transform spawnPointNow = spawnPoints[Random.Range(0, spawnPoints.Length)];
                    Vector3 positionNow = spawnPointNow.position;
                    positionNow.x += (Random.value - 0.5f) * spawnPointDiameter;
                    positionNow.z += (Random.value - 0.5f) * spawnPointDiameter;

                    GameObject prefab = (GameObject)GameObject.Instantiate(EnemyPrefab, positionNow, spawnPointNow.rotation);
                    //prefab.SendMessage("SetWaveSpawner", this);
                    enemiesLeft++;
                }
            }
            timeTilNextWave -= 1.0f;
            yield return new WaitForSeconds(1.0f);
        }
    }

    void OnDrawGizmos() {
        Gizmos.color = Color.blue;
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            Gizmos.DrawWireSphere(spawnPoints[i].position, spawnPointDiameter / 2.0f);
        }
    }

    void EndWave() {
        currentWave++;
        timeTilNextWave = timeBetweenWaves;
    }

    public void AgentDied() {
        enemiesLeft--;
        if (enemiesLeft <= 0)
        {
            EndWave();
        }
    }
}


    [System.Serializable]
    public class AIGroup
    {
        public GameObject[] agents;
    }
}