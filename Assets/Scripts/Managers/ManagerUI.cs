﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace SDF {
    public class ManagerUI : SingletonManager<ManagerUI> {
        public TextMeshProUGUI TextPopulationPoints;
        public TextMeshProUGUI TextIndustryPoints;
        public TextMeshProUGUI TextEnergyPoints;

        protected override void Start() {
            base.Start();
        }


        void Update() {

        }
    }
}