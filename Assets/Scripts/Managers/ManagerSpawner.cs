﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDF {
    public class ManagerSpawner : SingletonManager<ManagerSpawner> {

        [Header("Spawn Settings")] public Camera _MainCamera; //the main camera
        public float spawnBoxDistanceFromCamera = 15;
        public float spawnBoxSize = 3;

        [Header("Progression Settings")]
        public bool enableProgression = true;
        public float minTimeBetweenObstacles = 0.5f; //the miminum time between the spawning objects
        public float maxTimeBetweenObstacles = 3f; //the maximum time between the spawning objects

        public float currentTimeBetweenObstacles;
        //the current time based on the progression duration and the animation curve

        public float progressionDuration = 1f; //the progression duration

        public List<EnemyBrainLogic> EnemyBrainLogics = new List<EnemyBrainLogic>();
        public List<EnemyAttackLogic> EnemyAttackLogics = new List<EnemyAttackLogic>();
        public List<EnemyMovementLogic> EnemyMovementLogics = new List<EnemyMovementLogic>();



        private IEnumerator SpawnDestroyableObjectsConstantly() {

            while (ManagerGame.Instance.gameOver == false) {
                yield return
                    new WaitForSecondsRealtime(Random.Range(minTimeBetweenObstacles, currentTimeBetweenObstacles));

                if (ManagerGame.Instance.gameOver == false)
                    SpawnDestroyableObject();
            }
        }

        private void SpawnDestroyableObject() {
            //Lets get a random object to spawn.
            //DestroyableObject newDestroyableObject = PoolManager.singleton.GetObjectFromPool(destroyableObjects[(int)Random.Range(0, destroyableObjects.Count)]).GetComponent<DestroyableObject>();

            //(int)Random.Range(0, destroyableObjects.Count)


            ////Set a random position.
            ////We are spawning based on the middle point set in inspector and the boundaries of spawning.
            //Vector3 randomPosition = GetRandomPosition();
            ////After that we will set a random rotation for our object.
            //Quaternion randomRotation = Random.rotation;
            ////Set the movement direction.
            //Vector3 movementDirection = (target.transform.position - randomPosition).normalized;

            //newDestroyableObject.EnableClickableObject(randomPosition, randomRotation, movementDirection);
        }

        private Vector3 GetRandomPosition() {
            //Randomize between bottom/top or left/right
            int randomPosition = (int) Random.Range(0, 2);

            float[] extremitiesValues = new float[] {-0.1f, 1.1f};
            float randomXPos = 0, randomYPos = 0, randomZPos = 0;

            if (randomPosition == 0) {
                //We are spawning on bottom/top line
                //Get a random for x 0 is equal to the left side of the screen while 1 is the right side         
                randomYPos = extremitiesValues[(int) Random.Range(0, extremitiesValues.Length)];

                //Get a random pos for Y which means up or down
                randomXPos = Random.Range(0f, 1f);
            }
            else {
                //We are spawning on right/left line
                //Get a random for x 0 is equal to the left side of the screen while 1 is the right side         
                randomXPos = extremitiesValues[(int) Random.Range(0, extremitiesValues.Length)];

                //Get a random pos for Y which means up or down
                randomYPos = Random.Range(0f, 1f);
            }

            //Random z position
            randomZPos = Random.Range(spawnBoxDistanceFromCamera - spawnBoxSize/2,
                spawnBoxDistanceFromCamera + spawnBoxSize/2);

            //Return the random position
            return _MainCamera.ViewportToWorldPoint(new Vector3(randomXPos, randomYPos, randomZPos));
        }


        private void OnDrawGizmos() {
#if UNITY_EDITOR
            //Spawn settings visualised in the Editor
            Utilities.DrawSpawnBoxBasedOnCameraFrustum(_MainCamera, spawnBoxDistanceFromCamera, spawnBoxSize);
            Gizmos.color = Color.red;
#endif
        }
    }
}