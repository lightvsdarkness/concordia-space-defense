﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruction : MonoBehaviour {
    public float TimeBeforeDestruction = 5;
    // Start is called before the first frame update
    void Start() {
        StartCoroutine(DestroyItselfAfterTime(TimeBeforeDestruction));
    }


    IEnumerator DestroyItselfAfterTime(float timeBeforeRestart) {
        yield return new WaitForSeconds(timeBeforeRestart);
        Destroy(gameObject);
    }
}
