﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDF {
    public class ManagerInput : SingletonManager<ManagerInput> {
        public LayerMask MaskBoard;

        public LayerMask MaskExceptBoard;
        public LayerMask MaskInteractive;

        protected override void Start() {
            base.Start();
        }

        // Update is called once per frame
        void Update() {

        }
    }
}