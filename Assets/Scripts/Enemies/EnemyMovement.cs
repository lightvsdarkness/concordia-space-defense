using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace SDF
{
	public class EnemyMovement : MonoBehaviour {
        public EnemyBrain _EnemyBrain;

        [SerializeField] private float movementSpeed = 1f;
	    [SerializeField] private float rotationalDamp = 1.5f;

        [SerializeField] private float detectionDistance = 0.4f;
        [SerializeField] private float rayCastOffset = 0.2f;



        public void Turn() {
                Vector3 pos = _EnemyBrain.Target.position - transform.position;
                Quaternion rotation = Quaternion.LookRotation(pos);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationalDamp * Time.deltaTime);

		}

        public void Move() {
		    if (_EnemyBrain.Target != null) {
		        transform.position += transform.forward*movementSpeed*Time.deltaTime;
		    }
		}

        public void Pathfind() {
			RaycastHit hit;
			Vector3 rayCastVectorOffset = new Vector3();

			Vector3 left = transform.position - transform.right * rayCastOffset;
			Vector3 right = transform.position + transform.right * rayCastOffset;
			Vector3 up = transform.position + transform.up * rayCastOffset;
			Vector3 down = transform.position - transform.up * rayCastOffset;

			Debug.DrawRay(left, transform.forward * detectionDistance, Color.cyan);
			Debug.DrawRay(right, transform.forward * detectionDistance, Color.cyan);
			Debug.DrawRay(up, transform.forward * detectionDistance, Color.cyan);
			Debug.DrawRay(down, transform.forward * detectionDistance, Color.cyan);

			if(Physics.Raycast(left, transform.forward, out hit, detectionDistance))
			{
                rayCastVectorOffset += Vector3.right;
			}
			else if(Physics.Raycast(right, transform.forward, out hit, detectionDistance))
			{
                rayCastVectorOffset -= Vector3.right;
			}
			
			if(Physics.Raycast(up, transform.forward, out hit, detectionDistance))
                rayCastVectorOffset -= Vector3.up;
			else if(Physics.Raycast(right, transform.forward, out hit, detectionDistance))
                rayCastVectorOffset += Vector3.up;
			
			if(rayCastVectorOffset != Vector3.zero)
				transform.Rotate(rayCastVectorOffset * 5f * Time.deltaTime);
			else
				Turn();
		}

	}
}