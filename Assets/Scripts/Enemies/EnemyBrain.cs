using System.Collections.Generic;
using System.Collections;
using System.Linq;
using UnityEngine;

namespace SDF
{
	public class EnemyBrain : MonoBehaviour {
		public Transform Target;

	    public float FightingDistance = 0.4f;

		public EnemyMain _EnemyMain;
		public EnemyAttack _EnemyAttack;
		public EnemyMovement _EnemyMovement;

		private void Update() {
		    if (Target == null)
		        Target = FindTarget();

		    if (Target != null) {
                _EnemyAttack.TryToAttack(Target);

		        var dist = Vector3.Distance(Target.transform.position, transform.position);
                //Debug.Log("dist: " + dist);
                if (FightingDistance <= dist) {
                    _EnemyMovement.Pathfind();
                    _EnemyMovement.Turn();
                    _EnemyMovement.Move();
                }

            }


		}


        public Transform FindTarget() {
			var targets = FindObjectsOfType<PlanetSector>().ToList();
		    float distance = float.MaxValue;
            Transform enemyTarget = null;

		    foreach (var target in targets) {
                float dist = Vector3.Distance(target.transform.position, transform.position);
		        if (dist < distance) {
                    distance = dist;
		            enemyTarget = target.transform;
		        }
		    }

            return enemyTarget;
		}

	}
}