﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if DOTweenChecker
using DG.Tweening;
#endif

namespace SDF {
    [RequireComponent(typeof (Collider))]
    [RequireComponent(typeof (Rigidbody))]
    public class EnemyMain : MonoBehaviour, IDamageable {
        public float Health = 100;
        float IDamageable.Health => Health;


        private Vector3 initialScale = Vector3.one;
            // //The initial scale. This is used in case we've disable the random scale, but we enable de scale disparition effect

        public bool canBeClicked = true; //was this object clicked?

        [Header("On Enable Randomize Settings")] public float minMovementSpeed = 1; //min movement speed
        public float maxMovementSpeed = 3; //max movement speed
        private Vector3 movementDirection; //the direction of moveoment
        private float movementSpeed = 1; //the movement speed (radomized between min and max speed)

        public bool enableTumble = true; //Enable tumble (rotation)
        private Vector3 rotationDirection = Vector3.one;
        public float minRotationSpeed = 50;
        public float maxRotationSpeed = 120;
        private float currentRotationSpeed;

        public bool enableRandomScale = true; //Enable random scale
        public float minScale = 0.8f; //Min scale that this object might have
        public float maxScale = 1.5f; //Max scale that this object might have


        [Space(20)] [Header("On Destroy Settings")] 
        public GameObject Debris; //Prefab of the particles
        public bool enableDelayBeforeDestroying = true; //After you click on an object set a delay before destroying it
        public float delayToDestroyIt = 0.5f; //the delay

        public bool enableForceAppliedOnClick = true; //Would you like to apply force whenever you click on an object?
        public float forceAppliedOnClick = 50f; //The force to apply


        public bool enableDisparitionScaleEffect = false; //Enable scale effect when you click on an object
        public float disparitionScaleDuration = 0.5f; //Duration
#if DOTweenChecker
    public Ease disparitionScaleEase = Ease.Linear; //Scale ease
#endif



        [Space(20)] [Header("On hit target particle system")] public bool enableOnHitTargetParticleSystem;
            //Would you like to spawn a particle system when this object hits the target?

        public GameObject onHitTargetParticleSystem; //The On Hit Particle System
        public float onHitTargetParticleOffset = 3; //Set an offset for the particles position on the movement direction


        protected Rigidbody myRigidbody; //rigidbody
        protected Collider myCollider; //collider
        public Transform _Transform => transform;


        private void Awake() {
            myRigidbody = this.GetComponent<Rigidbody>();
            myCollider = this.GetComponent<Collider>();

            //Now lets make sure that we are not using gravity and it is not kinematic, so we can apply force.
            myRigidbody.isKinematic = true;
            myRigidbody.useGravity = false;
            //Also lets make sure that the collider isn't a trigger so the physics can happen.
            myCollider.isTrigger = false;


            initialScale = this.transform.localScale;

            //This object can be clicked
            canBeClicked = true;

            ////If we enabled the particles system, then spawn it.
            //if (enableParticleSystem)
            //{
            //    //If we've set the particle prefab then we will add it to our pool manager. 
            //    //Either way, we will have to disable the particle on click
            //    if (particleSystemToSpawn != null)
            //    {
            //        //Add to the pool manager.
            //        if (PoolManager.singleton != null)
            //        {
            //            PoolManager.singleton.AddObjectToPoolDictionary(particleSystemToSpawn.GetComponent<PoolObject>(), 1);
            //        }
            //        else
            //        {
            //            Debug.LogWarning("We couldn't find a pool manager to add our particle to spawn on destroy.");
            //        }
            //    }
            //    else
            //    {
            //        enableParticleSystem = false;
            //        Debug.LogWarning("You set Enable Particle System on, but you didn't set the Prefab.");
            //    }
            //}


            ////Also check the same things for OnHitTarget particle system
            //if (enableOnHitTargetParticleSystem)
            //{
            //    //If we've set the particle prefab then we will add it to our pool manager. 
            //    //Either way, we will have to disable the particle on hit target
            //    if (onHitTargetParticleSystem != null)
            //    {
            //        //Add to the pool manager.
            //        if (PoolManager.singleton != null)
            //        {
            //            PoolManager.singleton.AddObjectToPoolDictionary(onHitTargetParticleSystem.GetComponent<PoolObject>(), 1);
            //        }
            //        else
            //        {
            //            Debug.LogWarning("We couldn't find a pool manager to add our particle to spawn on hit the target.");
            //        }
            //    }
            //    else
            //    {
            //        enableOnHitTargetParticleSystem = false;
            //        Debug.LogWarning("You set the On Hit Target Enable Particle System on, but you didn't set the Prefab.");
            //    }
            //}
        }


        //private void Update() {
        //    //Make the object move in the set direction
        //    this.transform.Translate(movementDirection * movementSpeed * Time.deltaTime, Space.World);

        //    if (enableTumble)
        //    {
        //        this.transform.Rotate(rotationDirection * currentRotationSpeed * Time.deltaTime, Space.Self);
        //    }
        //}

        /// <summary>
        /// Sets a destroyable object.
        /// This function is called whenever a new destroyable object is needed.
        /// </summary>
        public void EnableClickableObject(Vector3 position, Quaternion rotation, Vector3 movementDir) {
            //This object is clickable now.
            this.canBeClicked = true;

            //Activate the pool object
            //this.myPoolObject.ActivatePoolObject();

            //Set the position, rotation and apply the force.
            this.transform.position = position;
            this.transform.rotation = rotation;
            this.movementDirection = movementDir;

            //Randomize the scale and tumble speed, if enabled.
            this.RandomizeTheObject();
        }

        /// <summary>
        /// Randomize the gameobject on enable.
        /// For the moment we are randomizing the scale and the tumble speed.
        /// If those are available.
        /// </summary>
        private void RandomizeTheObject() {
            //Randomize the scale
            if (enableRandomScale)
                this.transform.localScale = Vector3.one * UnityEngine.Random.Range(minScale, maxScale);
            else
                this.transform.localScale = initialScale;

            if (enableTumble) {
                rotationDirection = UnityEngine.Random.insideUnitSphere;

                currentRotationSpeed = UnityEngine.Random.Range(minRotationSpeed, maxRotationSpeed);
            }

            //Randomize the movement speed
            movementSpeed = UnityEngine.Random.Range(minMovementSpeed, maxMovementSpeed);
        }

        /// <summary>
        /// Destroys the object.
        /// </summary>
        public void DisableClickableObject() {
            ////If this object can be clicked then apply the settings set in the inspector for this event.
            //if (canBeClicked)
            //{
            //    //Don't let the user click on it anymore if he already clicked on this object.
            //    canBeClicked = false;

            //    //If particles are enabled then we know that we've spawned the particles in the Awake function. 
            //    //Now we will only have to positionate it and set them on.
            //    if (enableParticleSystem)
            //    {
            //        //Make sure that we've spawned them.
            //        if (particleSystemToSpawn != null)
            //        {
            //            //Make sure that we've spawned them.
            //            //Get the first inactive particle system from our pool manager.
            //            PoolParticleSystem particleSystemCaught = PoolManager.singleton.GetObjectFromPool(particleSystemToSpawn.GetComponent<PoolObject>()).GetComponent<PoolParticleSystem>();
            //            particleSystemCaught.EnableParticle(this.transform.position);
            //        }
            //    }

            //    //IF we enabled to apply force when the player clicks on an object, then just add the force on the camera's forward direction
            //    if (enableForceAppliedOnClick)
            //    {
            //        //Apply force. We are applying the force based on camera's forward direction, which is practically our forward direction.
            //        myRigidbody.AddForce(GameManager.singleton.mainCamera.transform.forward * forceAppliedOnClick);
            //    }

            //    //After that just apply the disparition effect 
            //    //Which might be a scale, or a just a gameobject active = false with a delay.
            //    ApplyDisparitionEffect();
            //}
        }


        /// <summary>
        /// Disparition basic effect.
        /// We are lerping the local scale (Vector3) from the current scale to zero, with a set duration.
        /// </summary>
        /// <returns></returns>
        private IEnumerator DisparitionEffectCoroutine() {
            //The percentage of the lerp
            float disparitionEffectPercentage = 0f;
            //The rate that we are adding each frame to the perecentage.
            float rate = 1/disparitionScaleDuration;

            //The start scale.
            Vector3 startScale = this.transform.localScale;
            //While the lerp isn't done, set the localscale of the current object based on the lerp percentage
            while (disparitionEffectPercentage < 1) {
                //Add the rate, each frame
                disparitionEffectPercentage += rate*Time.deltaTime;
                //Set the local scale
                this.transform.localScale = Vector3.Lerp(startScale, Vector3.zero, disparitionEffectPercentage);
                yield return null;
            }
        }

        /// <summary>
        /// Unity on collison enter callback.
        /// </summary>
        /// <param name="other">Collision</param>
        private void OnTriggerEnter(Collider other) {
            if (string.Equals(other.transform.tag, GameSettings.Tags.Destroyable)) {
                // 

            }
        }

        public void ReceiveDamage(float damageAmount, Vector3 hitPosition) {
            Health -= damageAmount;
            if (Health <= 0 )
                DestroyShip();
        }

        public void ReceiveHeal(float healAmount, Vector3 hitPosition) {

        }

        private void DestroyShip() {

            Instantiate(Debris, transform.position, Quaternion.identity);

            if (enableDelayBeforeDestroying) {
                Invoke("DestroyShipUmmediate", delayToDestroyIt);
            }

        }
        private void DestroyShipUmmediate() {
            Destroy(gameObject);
        }
    }



    /// <summary>
    /// Debris Info class.
    /// </summary>
    [System.Serializable]
    public class DebrisInfo {
        public GameObject debrisPrefab; //Debris prefab (we will add this to our pool manager)
        public bool hasFixedNumber = false; //Spawn a fixed number of this debris
        public int minNumberOfDebrisToSpawn = 2; //Min number to spawn
        public int maxNumberOfDebrisToSpawn = 4; //Max number to spawn
    }
}