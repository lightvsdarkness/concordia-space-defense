using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace SDF
{
	public class EnemyAttack : MonoBehaviour {
		public EnemyBrain _EnemyBrain;

		public IWeapon _Weapon;

        private void Start() {
            if (_Weapon == null)
                _Weapon = GetComponentInChildren<IWeapon>();
        }
        private void Update() {
            //Debug.DrawLine(transform.position, transform.position + transform.forward, Color.green);
        }

        public void TryToAttack(Transform target) {
            //Debug.Log("Enemy InFront: " + InFront());
            //Debug.Log("Enemy HaveLineOfSight: " + HaveLineOfSight());
            if (InFront() && HaveLineOfSight() && _Weapon.CanFire) {
				FireWeapon(target.position);
			}
		}

		public bool InFront() {
			Vector3 directionToTarget = _Weapon._Transform.position - _EnemyBrain.Target.position;
			float angle = Vector3.Angle(_Weapon._Transform.forward, directionToTarget);

			if (Mathf.Abs(angle) > 90 && Mathf.Abs(angle) < 270)
			{
				//Debug.DrawLine(transform.position, _EnemyBrain.Target.position, Color.green);
				return true;
			}

			return false;
		}

		public bool HaveLineOfSight() {
			RaycastHit hit;
			Vector3 direction = _EnemyBrain.Target.position - _Weapon._Transform.position;

			if (Physics.Raycast(_Weapon._Transform.position, direction, out hit, _Weapon.MaxDistance, ManagerInput.Instance.MaskExceptBoard)) {
				if (hit.transform.CompareTag(GameSettings.Tags.Player)) {
					//Debug.DrawRay(_Weapon._Transform.position, direction, Color.red);
					return true;
				}
			}

			return false;
		}

		private void FireWeapon(Vector3 targetPosition) {
			Debug.Log("Enemy " + transform.parent.name + " Firing _Weapon: ");
			_Weapon.TryFireWeapon(targetPosition);
		}

	}
}