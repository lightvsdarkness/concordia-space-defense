using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace SDF
{
    [RequireComponent(typeof (LineRenderer))]
    public class LaserAdvanced : MonoBehaviour, IWeapon {
        public bool Debugging;
        public float specialYUV;

        public bool CanFire = true;
        public bool Restarting = false;

        [SerializeField] private float MaxTimeLaserIsOn = 0.25f;
        [SerializeField] private float TimeBeforeRestart = 0.5f;

        [SerializeField] private float DefaultDamage = 10;
        public float MaxDistance = 100f;


        [Header("Visuals")]
        public float UpdateRate = 0.001f; // Animation time
        private WaitForSeconds WaitUpdateRate;

        public Texture[] BeamFrames; // Animation frame sequence
        public float beamScale = 1f; // Default beam scale to be kept over distance

        public bool AnimateUV = true; // UV Animation
        public float UVTime = -3f; // UV Animation speed
        private float animateUVTime;
        private float initialBeamOffset; // Initial UV offset 
        public Transform RayImpactEffect; // Impact transform

        private int frameNo; // Frame counter

        private Vector3 _targetPosition = Vector3.zero;

        [Header("Links")]
        public Transform MuzzleTransform;
        [SerializeField] private LineRenderer lineRend;

        public List<IDamageable> TargetsHit = new List<IDamageable>();
        private Coroutine CorrectBeamPositionRef = null;
        private Coroutine TurnOffWeaponRef = null;

        float IWeapon.MaxDistance => MaxDistance;

        bool IWeapon.CanFire => CanFire;

        public Transform _Transform => transform;


        private void Awake() {
            lineRend = GetComponent<LineRenderer>();
            WaitUpdateRate = new WaitForSeconds(UpdateRate);

            // Randomize uv offset
            initialBeamOffset = UnityEngine.Random.Range(-0.1f, 0.1f);
        }
        private void Start() {
            Initialize();
        }
        private void Update() {
            //Debug.DrawLine(transform.position, transform.position + transform.forward, Color.magenta);
            if (_targetPosition != Vector3.zero)
                Debug.DrawLine(transform.position, _targetPosition, Color.red);
        }


        private void Initialize() {
            lineRend.enabled = false;
            CanFire = true;
        }

        public void StopAttack() {
            if (TurnOffWeaponRef != null) {
                //if(Debugging)
                //    Debug.Log("Laser stopping TurnOffWeaponRef ", this);
                StopCoroutine(TurnOffWeaponRef);
                TurnOffWeapon();
            }

        }
        public void TryFireWeapon(Vector3 targetPosition, float WeaponBonus = 0f)
        {
            if (CanFire)
            {
                if (Debugging)
                    Debug.Log("Laser" + transform.parent.parent.parent + " FireWeapon at targetPosition: " + targetPosition, this);

                // It's 16 here
                VisualizeFiring(CastRay(targetPosition));
                // It's CastRay returning 14 - where it managed to hit target
            }
        }


        public Vector3 CastRay(Vector3 targetPosition)
        {
            //Debug.Log(gameObject.name + " CastingRay from: " + transform.position);
            //Debug.Log(gameObject.name + " CastingRay at targetPosition: " + targetPosition);
            //Debug.Log(gameObject.name + " CastingRay at transform.TransformPoint(targetPosition): " + transform.TransformPoint(targetPosition));
            RaycastHit hit;
            //transform.InverseTransformPoint(targetPosition)
            _targetPosition = targetPosition;

            Vector3 direction = targetPosition - transform.position;
            //if (Debugging)
                //Debug.DrawRay(transform.position, direction, Color.red, 1f);
                //Debug.Log("Laser of " + transform.parent.parent.parent + ManagerInput.Instance.MaskInteractive);
            if (Physics.Raycast(transform.position, direction, out hit, 1, ManagerInput.Instance.MaskInteractive))
            {
                Debug.Log("Laser of " + transform.parent.parent.parent + "Something is hit: " + hit.transform.name);
                var targetHit = hit.transform.GetComponent<IDamageable>();
                if (targetHit != null)
                {
                    if (Debugging)
                        Debug.Log("Laser of " + transform.parent.parent.parent + " Target hit: " + hit.transform.name);
                    TargetsHit.Add(targetHit);
                    Damage(DefaultDamage, hit.point);

                }
                    
                return hit.point;
            }
            else
            {
                Debug.Log("Laser of " + transform.parent.parent.parent +  " missed!");
                return transform.position + direction.normalized * MaxDistance;

            }
        }

        public void VisualizeFiring(Vector3 targetPosition)
        {
            if (CanFire)
            {
                CanFire = false;
                lineRend.enabled = true;
                //lineRend.SetPosition(0, MuzzleTransform.position);
                //lineRend.SetPosition(1, targetPosition);
                if (Debugging)
                    Debug.Log("Laser of " + transform.parent.parent.parent + " VisualizeFiring (Before Animate) at targetPosition: " + targetPosition);

                Animate(targetPosition);

                if (TurnOffWeaponRef != null)
                    StopCoroutine(TurnOffWeaponRef);
                TurnOffWeaponRef = StartCoroutine(TurnOffWeaponCor(MaxTimeLaserIsOn));
            }
        }

        void Animate(Vector3 targetPosition) {
            if (BeamFrames.Length > 1)
            {
                // Set current frame
                frameNo = 0;
                lineRend.material.mainTexture = BeamFrames[frameNo];

            }
            // Add timer. NOTE: Had to clean it up because if somehow new CorrectBeamPositionCor starts before previous one ends
            // then link to the previous one will be lost and it will never stop
            if (CorrectBeamPositionRef != null)
                StopCoroutine(CorrectBeamPositionRef);
            CorrectBeamPositionRef = StartCoroutine(CorrectBeamPositionCor(targetPosition));
        }

        void BeamFrameStep() {
            frameNo++;

            // Reset frame counter
            if (frameNo == BeamFrames.Length)
                frameNo = 0;

            // Set current texture frame based on frame counter
            lineRend.material.mainTexture = BeamFrames[frameNo];
        }


        public void Damage(float damageAmount, Vector3 hitPosition)
        {
            foreach (var targetHit in TargetsHit)
            {
                targetHit.ReceiveDamage(DefaultDamage, hitPosition);
            }
            TargetsHit.Clear();
        }

        public void TurnOffWeapon()
        {
            lineRend.enabled = false;

            if (CorrectBeamPositionRef != null) {
                if (Debugging)
                    Debug.Log("StopCoroutine(CorrectBeamPositionRef)", this);
                StopCoroutine(CorrectBeamPositionRef);
            }

            if (!Restarting)
                StartCoroutine(RestartWeaponCor(TimeBeforeRestart));
        }

        IEnumerator TurnOffWeaponCor(float timeBeforeTurningOff)
        {
            yield return new WaitForSeconds(timeBeforeTurningOff);

            TurnOffWeapon();
        }

        IEnumerator RestartWeaponCor(float timeBeforeRestart)
        {
            Restarting = true;
            yield return new WaitForSeconds(timeBeforeRestart);

            CanFire = true;
            Restarting = false;
        }

        IEnumerator CorrectBeamPositionCor(Vector3 targetPosition)
        {
            while (true)
            {
                //if(Debugging)
                //    Debug.Log("MuzzleTransform.position: " + MuzzleTransform.position);
                if (Debugging)
                    Debug.Log("Laser"+ transform.parent.parent.parent + " visualizing at targetPosition: " + targetPosition, this); //transform.InverseTransformDirection(targetPosition)
                // But it's 14 here

                lineRend.SetPosition(0, MuzzleTransform.position);
                lineRend.SetPosition(1, targetPosition);

                float beamLength = Vector3.Distance(transform.position, targetPosition);
                float propMult = beamLength * (beamScale / 10f);
                // Set beam scaling according to its length
                lineRend.material.SetTextureScale("_MainTex", new Vector2(propMult, 1f));

                // Animate texture UV
                if (AnimateUV)
                {
                    animateUVTime += Time.deltaTime;

                    if (animateUVTime > 1f)
                        animateUVTime = 0f;

                    lineRend.material.SetTextureOffset("_MainTex", new Vector2(animateUVTime * UVTime + initialBeamOffset, specialYUV));
                }

                // Adjust impact effect position
                if (RayImpactEffect)
                    RayImpactEffect.position = transform.position + transform.forward * beamLength;

                if (BeamFrames.Length > 1)
                    BeamFrameStep();

                yield return null;  //WaitUpdateRate
            }
        }


    }
}