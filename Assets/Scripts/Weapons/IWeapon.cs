using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SDF
{
	public interface IWeapon {
        float MaxDistance { get; }
        bool CanFire { get; }
        Transform _Transform { get; }


        void TryFireWeapon(Vector3 targetPosition, float WeaponBonus = 0f);
        void VisualizeFiring(Vector3 targetPosition);
        Vector3 CastRay(Vector3 targetPosition);
        void Damage(float damageAmount, Vector3 hitPosition);
        void StopAttack();
    }

    public enum WeaponType {
        Railgun,
        Rocket,
        ParticleBeam,
        Mines,
        Inteceptors
    }
}
