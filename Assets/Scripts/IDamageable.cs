using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SDF
{
	public interface IDamageable 
	{
        Transform _Transform { get; }

        float Health { get; }

		void ReceiveDamage(float damageAmount, Vector3 hitPosition);

        void ReceiveHeal(float healAmount, Vector3 hitPosition);

    }
}
