﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDF {
    public class SectorUI : MonoBehaviour {
        public PlanetSector PlanetSectorRef;

        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }


        public void AddIndustry() {
            ManagerPlanet.Instance.AddIndustry(PlanetSectorRef.SectorNumber - 1);
            
        }
        public void AddPowerPlant() {
            ManagerPlanet.Instance.AddPowerPlant(PlanetSectorRef.SectorNumber - 1);

        }


        public void AddWeaponParticleBeam() {
            Debug.Log("Add ParticleBeam");
            ManagerPlanet.Instance.AddWeaponParticleBeam(PlanetSectorRef.SectorNumber - 1);

        }
    }
}