﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchUI : MonoBehaviour {
    public bool State;
    void Start() {
        gameObject.SetActive(State);
    }


    public void SwitchState() {
        State = !State;
        gameObject.SetActive(State);
    }
}
